## Install Jenkins
### Prerequisites
Oracle JDK  installed
### Installing the Default JRE/JDK

`sudo apt update`

Execute the following command to install the default Java Runtime Environment (JRE), which will install the JRE from OpenJDK 11:

```
sudo apt install default-jre
```
The JRE will allow you to run almost all Java software.

Verify the installation with:
`java -version`


You may need the Java Development Kit (JDK) in addition to the JRE in order to compile and run some specific Java-based software. To install the JDK, execute the following command, which will also install the JRE:

```
sudo apt install default-jdk
```

Verify that the JDK is installed by checking the version of `javac`, the Java compiler:

```
javac -version
```

### Step 1 — Installing Jenkins using VM

Update Debian compatible operating systems (Debian, Ubuntu, Linux Mint Debian Edition, etc.) with the command:

Debian/Ubuntu LTS release

```
$ curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
$ echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
```

Package update and install jenkins
```
sudo apt update
sudo apt install jenkins
```

###  Step 2 — Starting Jenkins
```
sudo systemctl start jenkins
sudo systemctl enable jenkins
sudo systemctl status jenkins
```
### Step 3 — Setting Up Jenkins

To set up your installation, visit Jenkins on its default port, `8080`, using your server domain name or IP address: `http://your_server_ip_or_domain:8080`


In the terminal window, use the `cat` command to display the password:

```
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```

Copy the 32-character alphanumeric password from the terminal and paste it into the **Administrator password** field, then click **Continue**.

The next screen presents the option of installing suggested plugins or selecting specific plugins:
We’ll click the **Install suggested plugins** option, which will immediately begin the installation process.


Now setup username password etc and login Jenkins.


## Installing Jenkins using docker container based of ubuntu Image

Jenkins Dockerfile.yaml

```
#Importing base image Ubuntu
FROM ubuntu:22.04
ENV DEBIAN_FRONTEND noninteractive
#Updating and Upgrading Ubuntu
RUN apt-get -y update \
&& apt-get -y upgrade
#Installing Basic Packages & Utilities in Ubuntu
RUN apt-get -y install software-properties-common git gnupg sudo nano vim wget curl zip unzip build-essential libtool autoconf uuid-dev pkg-config libsodium-dev lynx-common tcl inetutils-ping net-tools ssh openssh-server openssh-client openssl letsencrypt apt-transport-https telnet locales gdebi lsb-release
#Clear cache
RUN apt-get clean
#Jenkins Prerequisites
RUN sudo apt search openjdk
#Install Java version 11 as prerequisite
RUN apt-get -y install openjdk-11-jdk
#Jenkins installation
#Download & add repository key
RUN curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
 /usr/share/keyrings/jenkins-keyring.asc > /dev/null
#Getting binary file into /etc/apt/sources.list.d
RUN echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
#Updating packages
RUN sudo apt-get -y update
#Installing Jenkins
RUN sudo apt-get -y install jenkins
#Start jenkins
RUN service jenkins start
#Expose port 8080
EXPOSE 8080
#Run jenkins Service
ENTRYPOINT service jenkins start && /bin/bash

```

jenkins docker-compose.yaml file
```
version: "3.8"
services:
  jenkins:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "8086:8080"
    container_name: jenkins_compose
    restart: always
    tty: true
    volumes:
      - jenkins-data:/var/jenkins_home
volumes:
  jenkins-data:

```

Docker compose up
`docker compose -f docker-compose.yml up --build -d`

Login docker container:
`docker exec -it $container_ID bash`

## Integration of jenkins with gitlab

**Step 1**: Login to Jenkins Account and install the required plugin.

-   Git plugin
-   Git lab
-   Git lab API
-   Git lab Authentication
-   Credentials plugin

**Step 2**: Now create an GitLab access token for the integration of Jenkins to GitLab and save the token at any safe place.

Login gitlab click profile> and then edit profile > Click Access Tokens 
Toekn Name: ex. jenkins_git_access

Select Scopes: api

Create Personal Access token..
![[Access token.png]]



**Step 3**: Now Login On Jenkins Server open Manage jenkins -> Manage credentials -> Add GitLab  credentials to Jenks
Select Gitlab API token


![[Add_Credentials_Into_Jenkins.png]]




**Step 4**: Go to Manage Jenkins -> Configure system for GitLa and Jenkins Server Connection Test via API Token
![[Pasted image 20230416144519.png]]

Add Gitlab configuration information, used API Token as Credentials and Test Connections.
![[Pasted image 20230416144537.png]]

**Step 6**: From  Containarized Jenkins Server generate ssh-key for Jenkins user connection with GitLab

Login Jenkins Server, then generate ssh key

```
sudo su - jenkins
ssh-keygen
```

**Step 7**: Login to your GitLab Account add Jenkins Server SSH  Public key in Gitlab SSH-Key Setting options. User Settings > Edit Profile> SSH Keys

![[Pasted image 20230416145126.png]]

Example adding Jenkins Server public Key ‘id_rsa.pub’ key to Gitlab SSH Keys account

![[Pasted image 20230416145148.png]]


**Step 08**: Add Jenkins Server Private Key to Jenkins Web UI in GlobalCredentails sections ‘id_rsa’ private key to Jenkins Server.
Manage Jenkins > Credentials > System > Global credentials > New credentials
*User Name should be git*
![[Pasted image 20230416145219.png]]


**Step 11**: For Jenkins Server and GitLab communication, create one New project in Gitlab (note: untick in project configuration section to Initialize repositry with a README).

![[Pasted image 20230416145316.png]]

Click Create Project

From jenkins server from jenkins user inster below instrcutions commands for check git remote repo is accessable.

##### Git global setup

```
git config --global user.name "nasirsm"
git config --global user.email "nasir@shurjomukhi.com.bd"
```


##### Clone  a Git repository
In container jenkins user verifying gitlab ssh connection\
```
git@gitlab.com:adorsho-pranisheba/cms/apsshop.com.bd-cms.git
```


**Steps 12**: Now generate ssh-keygen from Jenkins Server and copy ssh public key to Application Server for jenkins user login to application server without password.

Login Jenkins server and switch to Jenkins user, after login container make sure suitch jenkins user `sudo su - jenkins`

```

ssh-keygen
cd .ssh
ssh-copy-id -i id_rsa.pub jenkins@application-server-ip
```

Now login application server using Deployer user and added below line for execute command without sudo permission.

`echo "jenkins ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers`

or 

`sudo visudo`
jenkins ALL=(ALL) NOPASSWD: ALL
save it.

Added deployer user to docker group for execute docker command without sudo comand
	`sudo vim /etc group`
	docker: jenkins

save it.

**Steps 13**: Create Jenkins file and push it to gitlab remote project directory

`vim Jenkinsfile`
```
pipeline {
  agent any
  stages {
    stage('Backup current project directory') {
      steps {
        sh 'ssh jenkins@116.212.111.45 "tar -cvf sharedfarm_$(date +%Y-%m-%d-%H.%M.%S).tar /var/www/cattle_weight_backend"'
		    sh 'ssh jenkins@116.212.111.45 "sudo mv sharedfarm*.tar /home/jenkins/backup"'
        sh 'ssh jenkins@116.212.111.45 "cd /home/jenkins/backup && ls -t | tail -n +3 | xargs sudo rm -f"'
      }
    }
    stage('Chnage directory permission for scp') {
        steps {
        sh 'ssh jenkins@116.212.111.45 "sudo chown -R jenkins:jenkins /var/www/cattle_weight_backend"' 
        }
    }
    stage('Copy changes to production server') {
      steps {
        sh 'scp -pr * jenkins@116.212.111.45:/var/www/cattle_weight_backend'
      }
    }
    stage('Deploy with Docker Compose') {
      steps {
        sh 'ssh jenkins@116.212.111.45 "cd /var/www/cattle_weight_backend && docker compose -f docker-compose.yml down"'
        sh 'ssh jenkins@116.212.111.45 "cd /var/www/cattle_weight_backend && docker compose -f docker-compose.yml up --build -d"'
      }
    }
  }
}
```

**Steps 15**: Login to Jenkins Server create  pipeline job

Enter an itme name: your project name > select pipeline and click ok.

![[pipe-line.png]]
From Definition select pipeline script from SCM > SCM > Git
Repositories URL: provide your porject repository SSH URL.
Credentials: select your jenkins private key credentials
Branchs to build: */main  (provide branch name)
Script Path: Jenkinsfile *It's already have on your project directoy*
Apply & save it.
Now click to Build now for build this project.

![[pull-scm.png]]

From build triggers section check Poll SCM and add Schedule, Example: for every minute:\
` * * * * `



## References

Oracle JDK Install: https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-20-04#installing-specific-versions-of-openjdk

Jenkins updated repository keys: https://www.jenkins.io/blog/2023/03/27/repository-signing-keys-changing/

Jenkins Install: https://www.digitalocean.com/community/tutorials/how-to-install-jenkins-on-ubuntu-20-04

Integraed jenkins with gitlab:
1. Content:  https://www.eternalsoftsolutions.com/blog/integration-of-jenkins-with-gitlab/
2. Video Tuterial : https://www.youtube.com/watch?v=fE741bkK1kA

Jenkins installation in Ubuntu & Customized Docker Image
https://medium.com/@syedmuhammadarslan_80326/jenkins-installation-in-ubuntu-customized-docker-image-e6f722d97af0